package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    Map<String, Double> pricing;

    public Quoter() {
        pricing = new HashMap<String, Double>();
        pricing.put("1",10.0);
        pricing.put("2",45.0);
        pricing.put("3",20.0);
        pricing.put("4",35.0);
        pricing.put("5",50.0);
    }

    public double getBookPrice(String isbn){
        return pricing.getOrDefault(isbn, 0.0);
    }
}
